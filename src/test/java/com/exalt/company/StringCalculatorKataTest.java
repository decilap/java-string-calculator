package com.exalt.company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.*;

public class StringCalculatorKataTest {

    private StringCalculatorKata stringCalculatorKata;

    @BeforeEach
    public void initCalculatorKata(){
        stringCalculatorKata = new StringCalculatorKata();
    }

    @AfterEach
    public void undefinedCalculatorKata(){
        stringCalculatorKata = null;
    }

    @Test
    public void add_shouldReturnsZeroWhenStringEmpty(){
        Assertions.assertEquals(0, stringCalculatorKata.add(""));
    }

    @Test
    public void add_ShouldReturnsValueIfStringANumber(){
        Assertions.assertEquals(1, stringCalculatorKata.add("1"));
    }

    @Test
    public void add_addShouldReturnsTheSumIfTwoNumbers(){
        Assertions.assertEquals(3, stringCalculatorKata.add("1,2"));
    }

    @Test
    public void add_addShouldReturnsSumIfStringUnlimitedNumbers(){
        Assertions.assertEquals(14, stringCalculatorKata.add("1,2,4,7"));
    }

    @Test
    public void add_shouldHandlesNewLinesBetweenNumbers(){
        Assertions.assertEquals(6, stringCalculatorKata.add("1\n2,3"));
    }

    @Test
    public void add_shouldAcceptNewDelimiter(){
        Assertions.assertEquals(3, stringCalculatorKata.add("//;\n1;2"));
    }

    @Test
    public void add_shouldThrowExceptionWhenNegativeNumbers (){
        NumberFormatException thrown = Assertions.assertThrows(NumberFormatException.class, () -> {
            stringCalculatorKata.add("1,-2,-3");
        }, "negatives not allowed 1,-2,-3");
        Assertions.assertEquals("negatives not allowed -2, -3", thrown.getMessage());
    }
}
