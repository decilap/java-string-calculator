package com.exalt.company;



import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringCalculatorKata {

    int add(String numbers) {
        if(!numbers.isEmpty()) {
            String delimiter = "[,\n]";

            if (numbers.startsWith("//")) {
                delimiter="[\n"+numbers.charAt(2)+"]";
                numbers = numbers.substring(4);
            }

            String negativeNumbers = Stream.of(numbers.split(delimiter))
                    .filter(n -> Integer.parseInt(n) < 0)
                    .map(String::valueOf)
                    .collect(Collectors.joining(", "));

            if(!negativeNumbers.isEmpty()){
                throw new NumberFormatException("negatives not allowed " + negativeNumbers);
            }

            return  Stream.of(numbers.split(delimiter))
                    .map(s -> Integer.parseInt(s.trim()))
                    .mapToInt(i -> i).sum();
        }

        return 0;
    }
}
